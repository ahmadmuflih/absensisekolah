<?php
  $this->load->view('admin/header_v');
  $this->load->view('admin/sidebar_v') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Tambah Data Siswa</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Master Data</a></div>
        <div class="breadcrumb-item">Tambah Data Siswa</div>
      </div>
    </div>

    <div class="section-body">


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <form id="form">
                <div class="card-header">
                  <h4>Isi data</h4>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label>NISN</label>
                    <input type="number" name="nisn" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="name" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control">
                  </div>
                  <div class="form-group">
                    <label>Kelas</label>
                    <select name="class" class="form-control" required>
                      <option value="" selected disabled>- Pilih Kelas -</option>
                      <?php
                        foreach ($class->result() as $row) {
                          // code...
                          echo "<option value='$row->id'>$row->name</option>";
                        }
                      ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Card ID</label>
                    <input type="text" name="card_id" class="form-control" required>
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
$('#form').submit(function(){
      var form = $('#form')[0]; // You need to use standart javascript object here
      var formData = new FormData(form);
      $.ajax({
        url: '<?php echo base_url("admin/insertsiswa");?>',
        data: formData,
        type: 'POST',
        // THIS MUST BE DONE FOR FILE UPLOADING
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
          if (data.status=='success') {
            swal("Berhasil!", data.message, "success");
            $('#form')[0].reset();
          }else {
            swal("Gagal!", data.message, "error");
          }
        },
            error: function(jqXHR, textStatus, errorThrown)
            {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        alert('gagal');
      }
      })
      return false;
  });
</script>

<?php $this->load->view('admin/footer_v'); ?>
