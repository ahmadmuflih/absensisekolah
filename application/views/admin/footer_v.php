<footer class="main-footer">
  <div class="footer-left">
    Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
  </div>
  <div class="footer-right">
    2.3.0
  </div>
</footer>
</div>
</div>



<!-- JS Libraies -->
<script src="<?php echo base_url()?>node_modules/simpleweather/jquery.simpleWeather.min.js"></script>
<script src="<?php echo base_url()?>node_modules/chart.js/dist/Chart.min.js"></script>
<script src="<?php echo base_url()?>node_modules/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?php echo base_url()?>node_modules/jqvmap/dist/maps/jquery.vmap.world.js"></script>
<script src="<?php echo base_url()?>node_modules/summernote/dist/summernote-bs4.js"></script>
<script src="<?php echo base_url()?>node_modules/chocolat/dist/js/jquery.chocolat.min.js"></script>

<script src="<?php echo base_url()?>assets/js/scripts.js"></script>
<script src="<?php echo base_url()?>assets/js/custom.js"></script>
</body>
</html>
