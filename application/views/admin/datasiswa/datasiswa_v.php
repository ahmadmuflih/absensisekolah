<?php
  $this->load->view('admin/header_v');
  $this->load->view('admin/sidebar_v') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Master Data Siswa</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Master Data</a></div>
        <div class="breadcrumb-item">Data Siswa</div>
      </div>
    </div>

    <div class="section-body">


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="col-4">
                    <h4>List Siswa</h4>
                </div>
                <div class="col-8">
                    <a href="<?php echo base_url() ?>admin/tambahsiswa" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add Data</a>
                </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="form-group col-md-3">
                  <label>Filter Kelas</label>
                  <select id="class" class="form-control">
                    <option value="0" selected>- Semua -</option>
                    <?php
                      foreach ($class->result() as $row) {
                        // code...
                        if($row->id == $student->class_id)
                          echo "<option value='$row->id' selected>$row->code - $row->name</option>";
                        else
                          echo "<option value='$row->id'>$row->code - $row->name</option>";
                      }
                    ?>
                  </select>
                </div>
              </div>
              <div class="table-responsive">


                <table class="table table-striped" id="mytable">
                  <thead>
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>NISN</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <th>Card ID</th>
                      <th>Kelas</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
var class_id = $('#class').val();
$('#class').on('change',function(){
  class_id = $(this).val();
  loadData();
});
loadData();
function loadData(){
          $('#mytable').DataTable().destroy();
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                      {
                          return {
                              "iStart": oSettings._iDisplayStart,
                              "iEnd": oSettings.fnDisplayEnd(),
                              "iLength": oSettings._iDisplayLength,
                              "iTotal": oSettings.fnRecordsTotal(),
                              "iFilteredTotal": oSettings.fnRecordsDisplay(),
                              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                          };
                      };

                      t = $("#mytable").DataTable({
                          initComplete: function() {
                              var api = this.api();
                              $('#mytable_filter input')
                                      .off('.DT')
                                      .on('keyup.DT', function(e) {
                                          if (e.keyCode == 13) {
                                              api.search(this.value).draw();
                                  }
                              });
                          },
                          oLanguage: {
                              sProcessing: "loading..."
                          },
                          processing: true,
                          serverSide: true,
                          select: true,
                          ajax: {"url": "<?php echo base_url("admin/json_siswa");?>", "type": "POST","data":{"class_id":class_id}},
                          "columnDefs": [
              {
                  "targets": [ -1 ], //last column
                  "orderable": false, //set not orderable
              },
              ],
                          columns: [
                              {
                                  "data": "id",
                                  "orderable": false
                              },
                              {"data": "nisn"},
                              {"data": "name"},
                              {"data": "email"},
                              {"data": "card_id"},
                              {"data": "class"},
                              {"data": "view"}
                          ],
                          order: [[1, 'asc']],
                          rowCallback: function(row, data, iDisplayIndex) {
                              var info = this.fnPagingInfo();
                              var page = info.iPage;
                              var length = info.iLength;
                              var index = page * length + (iDisplayIndex + 1);
                              $('td:eq(0)', row).html(index);
                          }
                      });


          }
          function remove(id) {
        swal({
            title: 'Yakin?',
            text: 'Data siswa akan terhapus!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "<?php echo base_url() ?>admin/deletesiswa",
                    type: "POST",
                    dataType: "JSON",
                    data:{'id':id},
                    success: function(data)
                    {
                        if (data.status=='success') {
                            swal("Success!", data.message, "success");
                            reload_table();
                        }else {
                            swal("Failed!", data.message, "error");
                        }
                        console.log();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {

            }
        });
    }
    function reload_table()
{
  $('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
</script>

<?php $this->load->view('admin/footer_v'); ?>
