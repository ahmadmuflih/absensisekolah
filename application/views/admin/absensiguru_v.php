<?php
  $this->load->view('admin/header_v');
  $this->load->view('admin/sidebar_v') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Absensi Guru</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Absensi</a></div>
        <div class="breadcrumb-item">Absensi Guru</div>
      </div>
    </div>

    <div class="section-body">


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="col-4">
                    <h4>List Absensi Guru</h4>
                </div>
            </div>
            <div class="card-body">
              <div class="row">
                  <div class="col-lg-4">
                      <div class="form-group">
                          <label class="control-label">Filter Guru:</label>
                          <select id="teacher" type="text" class="form-control select2">
                                  <option value="0">ALL</option>
                                  <?php foreach ($teacher->result() as $row){
                                    echo "<option value='$row->id'>$row->name</option>";
                                  }?>

                              </select>

                      </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="form-group">
                          <label class="control-label">Filter Tanggal:</label>
                          <input type="text" class="form-control" id="datepicker">
                      </div>
                  </div>
              </div>
              <div class="table-responsive">
                <table class="table table-striped" id="mytable">
                  <thead>
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>NIP</th>
                      <th>Nama Guru</th>
                      <th>Ruangan</th>
                      <th>Waktu</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
var teacher_id = 0, date = 0;
$('#teacher').change(function(){
  teacher_id = $(this).val();
  loadData();
});
$('#datepicker').daterangepicker({
  autoUpdateInput: false,
  singleDatePicker: true,
  }, function(chosen_date) {
    date = chosen_date.format('YYYY-MM-DD');
    $('#datepicker').val(chosen_date.format('DD-MM-YYYY'));
    loadData();
});
loadData();
function loadData(){
  $('#mytable').DataTable().destroy();
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                      {
                          return {
                              "iStart": oSettings._iDisplayStart,
                              "iEnd": oSettings.fnDisplayEnd(),
                              "iLength": oSettings._iDisplayLength,
                              "iTotal": oSettings.fnRecordsTotal(),
                              "iFilteredTotal": oSettings.fnRecordsDisplay(),
                              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                          };
                      };

                      t = $("#mytable").DataTable({
                          initComplete: function() {
                              var api = this.api();
                              $('#mytable_filter input')
                                      .off('.DT')
                                      .on('keyup.DT', function(e) {
                                          if (e.keyCode == 13) {
                                              api.search(this.value).draw();
                                  }
                              });
                          },
                          oLanguage: {
                              sProcessing: "loading..."
                          },
                          processing: true,
                          serverSide: true,
                          select: true,
                          ajax: {"url": "<?php echo base_url("admin/json_absensi_guru");?>", "type": "POST","data":{
                            'teacher_id' : teacher_id,
                            'date' : date
                          }},
                          "columnDefs": [
              {
                  "targets": [ -1 ], //last column
                  "orderable": false, //set not orderable
              },
              ],
                          columns: [
                              {
                                  "data": "id",
                                  "orderable": false
                              },
                              {"data": "nip"},
                              {"data": "name"},
                              {"data": "room"},
                              {"data": "time"},
                              {"data": "view"}
                          ],
                          order: [[4, 'desc']],
                          rowCallback: function(row, data, iDisplayIndex) {
                              var info = this.fnPagingInfo();
                              var page = info.iPage;
                              var length = info.iLength;
                              var index = page * length + (iDisplayIndex + 1);
                              $('td:eq(0)', row).html(index);
                          }
                      });


          }
          function remove(id) {
        swal({
            title: 'Yakin?',
            text: 'Data absensi akan dihapus!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "<?php echo base_url() ?>admin/deleteabsensiguru",
                    type: "POST",
                    dataType: "JSON",
                    data:{'id':id},
                    success: function(data)
                    {
                        if (data.status=='success') {
                            swal("Success!", data.message, "success");
                            reload_table();
                        }else {
                            swal("Failed!", data.message, "error");
                        }
                        console.log();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {

            }
        });
    }
    function reload_table()
{
  $('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}



</script>

<?php $this->load->view('admin/footer_v'); ?>
