<?php
  $this->load->view('admin/header_v');
  $this->load->view('admin/sidebar_v') ?>
  <style>
  .hidden{
    display: none;
}
  </style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Master Data Siswa</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Master Data</a></div>
        <div class="breadcrumb-item">Data Siswa</div>
      </div>
    </div>

    <div class="section-body">


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="col-8">
                  <div class="row">
                    <div class="form-group">
                      <label>Filter Kelas</label>
                      <select id="class" class="form-control">
                        <option value="0">- Pilih Kelas -</option>
                        <?php
                          foreach ($class->result() as $row) {
                            // code...
                              echo "<option value='$row->id'>$row->code - $row->name</option>";
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-4">
                    <button type="button" onclick="addData()" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add Data</button>
                </div>
            </div>
            <div class="card-body">
              <div id="selected" class="hidden">
                <h4>Jadwal Kelas</h4>
                <div class="table-responsive">
                  <table class="table table-striped" id="mytable">
                    <thead>
                      <tr>
                        <th class="text-center">
                          #
                        </th>
                        <th>Nama Pelajaran</th>
                        <th>Hari</th>
                        <th>Waktu Mulai</th>
                        <th>Waktu Selesai</th>
                        <th>Guru</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="schedule_table">

                    </tbody>
                  </table>
                </div>
                <h4 class="hidden">Roster</h4>
                <div class="table-responsive hidden">
                  <table class="table table-bordered" id="mytable2">
                    <thead>
                      <tr>
                        <th>Jam</th>
                        <th>Senin</th>
                        <th>Selasa</th>
                        <th>Rabu</th>
                        <th>Kamis</th>
                        <th>Jumat</th>
                        <th>Sabtu</th>
                        <th>Minggu</th>
                      </tr>
                    </thead>
                    <tbody id="schedule_time">

                    </tbody>
                  </table>
                </div>
              </div>
              <div id="unselected">
                <h4><center>Pilih kelas terlebih dahulu</center></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal inmodal" id="myModal" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content animated fadeInDown">
        <form id="form">
        <div class="modal-header">
            <h4 class="modal-title">Tambah Jadwal Kelas</h4>
            <!-- <small>Pastikan data yang diisi telah sesuai </small> -->
        </div>
        <div class="modal-body">

              <div class="form-group">
                <label>Kelas</label>
                <input type="text" name="class" id="class_name" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label>Nama Pelajaran</label>
                <input type="text" name="name" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Hari</label>
                <select name="day" class="form-control" required>
                  <option selected disabled>- Pilih Hari -</option>
                  <option value="1">Senin</option>
                  <option value="2">Selasa</option>
                  <option value="3">Rabu</option>
                  <option value="4">Kamis</option>
                  <option value="5">Jumat</option>
                  <option value="6">Sabtu</option>
                  <option value="7">Minggu</option>
                </select>
              </div>
              <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                  <label>Jam Mulai</label>
                  <input type="text" name="start_time" class="clockpicker form-control" readonly>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Jam Selesai</label>
                  <input type="text" name="end_time" class="clockpicker form-control" readonly>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Guru</label>
              <select name="teacher" class="form-control" style="width:100% !important;display:block !important;">
                <option selected disabled> - Pilih Guru - </option>
                <?php
                  foreach($teachers->result() as $teacher){
                    echo "<option value='$teacher->id'>$teacher->name</option>";
                  }
                ?>
              </select>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
      </div>
  </div>
</div>
<div class="modal inmodal" id="myModalEdit" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content animated fadeInDown">
        <form id="form_edit">
        <div class="modal-header">
            <h4 class="modal-title">Edit Jadwal Kelas</h4>
            <!-- <small>Pastikan data yang diisi telah sesuai </small> -->
        </div>
        <div class="modal-body">

              <div class="form-group">
                <label>Kelas</label>
                <input type="text" name="class" id="class_name_edit" class="form-control" disabled>
              </div>
              <div class="form-group">
                <label>Nama Pelajaran</label>
                <input type="text" name="name" id="name" class="form-control" required>
              </div>
              <div class="form-group">
                <label>Hari</label>
                <select name="day" id="day" class="form-control" required>
                  <option selected disabled>- Pilih Hari -</option>
                  <option value="1">Senin</option>
                  <option value="2">Selasa</option>
                  <option value="3">Rabu</option>
                  <option value="4">Kamis</option>
                  <option value="5">Jumat</option>
                  <option value="6">Sabtu</option>
                  <option value="7">Minggu</option>
                </select>
              </div>
              <div class="form-group">
                <div class="row">
                <div class="col-md-6">
                  <label>Jam Mulai</label>
                  <input type="text" name="start_time" id="start_time" class="clockpicker form-control" readonly>
                  </select>
                </div>
                <div class="col-md-6">
                  <label>Jam Selesai</label>
                  <input type="text" name="end_time" id="end_time" class="clockpicker form-control" readonly>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>Guru</label>
              <select name="teacher" id="teacher" class="form-control" style="width:100% !important;display:block !important;">
                <option selected disabled> - Pilih Guru - </option>
                <?php
                  foreach($teachers->result() as $teacher){
                    echo "<option value='$teacher->id'>$teacher->name</option>";
                  }
                ?>
              </select>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
      </div>
  </div>
</div>

<script type="text/javascript">
var class_id = $('#class').val();
var selected_id;
$('.clockpicker').clockpicker({
  autoclose:true
});
$('#class').on('change',function(){
  class_id = $(this).val();
  if(class_id != '0')
  {
    $('#unselected').addClass('hidden');
    $('#selected').removeClass('hidden');
    loadData();
  }
  else{
    $('#unselected').removeClass('hidden');
    $('#selected').addClass('hidden');
  }

});
loadData();
function addData(){
  if(class_id == '0'){
    alert('Pilih kelas terlebih dahulu!');
  }
  else{
    $('#class_name').val($("#class option:selected").html());
    $('#myModal').modal('show');
  }
}
function loadData(){
  $.ajax({
            url: '<?php echo base_url("AJAX/getSchedule");?>',
            data: {'class_id':class_id},
            type: 'GET',
            dataType: "JSON",
            success: function(data){
              console.log(data);
              var html_data = "";
              if(data.list.length==0){
                html_data = "<tr><td colspan='7'><center>Tidak ada data!</center></td></tr>";
              }else{
                for(var i = 0; i < data.list.length; i++){
                  var teacher = (data.list[i].teacher!=null)?data.list[i].teacher:"";
                  html_data += "<tr><td>"+(i+1)+"</td><td>"+data.list[i].lesson+"</td><td>"+data.list[i].day+"</td>"+
                  "<td>"+data.list[i].start_time+"</td><td>"+data.list[i].end_time+"</td><td>"+teacher+"</td>"+
                  "<td><button type='button' class='btn btn-sm btn-info' onclick='edit("+data.list[i].id+")'>Edit</button> <button type='button' class='btn btn-sm btn-danger' onclick='hapus("+data.list[i].id+")'>Delete</button></td></tr>";
                }
              }
              $('#schedule_table').html(html_data);
              var table = '';
              for(var i = 6; i<18; i++){
                  table+='<tr><td><center>'+i+':00 - '+(i+1)+':00</center></td>';

                  for(var j = 1; j<=7; j++){
                    if((data.slot[j]||[])[i] === undefined){
                      table+='<td></td>';
                    }
                    else{
                      table+='<td style="background-color: #8be0be !important;"><center>'+data.slot[j][i]+'</center></td>';
                    }
                  }
                  table+='</tr>';
                }
              $('#schedule_time').html(table);
            }
        });
}
function hapus(id){
  swal({
      title: 'Yakin?',
      text: 'Data jadwal kelas akan terhapus!',
      icon: 'warning',
      buttons: true,
      dangerMode: true,
  }).then((willDelete) => {
      if (willDelete) {
          $.ajax({
              url : "<?php echo base_url() ?>admin/deletejadwal",
              type: "POST",
              dataType: "JSON",
              data:{'id':id},
              success: function(data)
              {
                  if (data.status=='success') {
                      swal("Success!", data.message, "success");
                      loadData();
                  }else {
                      swal("Failed!", data.message, "error");
                  }
                  console.log();
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  console.log(jqXHR);
                  console.log(textStatus);
                  console.log(errorThrown);
              }
          });
      } else {

      }
  });
}
function edit(id){
  selected_id = id;
  $.ajax({
            url: '<?php echo base_url("AJAX/getScheduleDetail");?>',
            data: {'schedule_id':id},
            type: 'GET',
            dataType: "JSON",
            success: function(data){
              var data = data.data;
              $('#name').val(data.name);
              $('#day').val(data.day_id);
              $('#start_time').val(data.start_time);
              $('#end_time').val(data.end_time);
              $('#teacher').val(data.teacher_id);
                console.log(data);
            }
        });
  $('#class_name_edit').val($("#class option:selected").html());
  $('#myModalEdit').modal('show');
}
$('#form').submit(function(){
      var form = $('#form')[0]; // You need to use standart javascript object here
      var formData = new FormData(form);
      formData.append('class_id',class_id);
      $.ajax({
        url: '<?php echo base_url("admin/insertjadwal");?>',
        data: formData,
        type: 'POST',
        // THIS MUST BE DONE FOR FILE UPLOADING
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data){
          if (data.status=='success') {
            swal("Berhasil!", data.message, "success");
            $('#form')[0].reset();
            loadData();
            // $('#myModal').modal('hide');
          }else {
            swal("Gagal!", data.message, "error");
          }
        },
            error: function(jqXHR, textStatus, errorThrown)
            {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
        alert('gagal');
      }
      })
      return false;
  });
  $('#form_edit').submit(function(){
        var form = $('#form_edit')[0]; // You need to use standart javascript object here
        var formData = new FormData(form);
        formData.append('class_id',class_id);
        formData.append('id',selected_id);
        $.ajax({
          url: '<?php echo base_url("admin/updatejadwal");?>',
          data: formData,
          type: 'POST',
          // THIS MUST BE DONE FOR FILE UPLOADING
          contentType: false,
          processData: false,
          dataType: "JSON",
          success: function(data){
            if (data.status=='success') {
              swal("Berhasil!", data.message, "success");
              $('#form')[0].reset();
              loadData();
              // $('#myModal').modal('hide');
            }else {
              swal("Gagal!", data.message, "error");
            }
          },
              error: function(jqXHR, textStatus, errorThrown)
              {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
          alert('gagal');
        }
        })
        return false;
    });

</script>

<?php $this->load->view('admin/footer_v'); ?>
