<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Teacher extends CI_Model {
  private $tableteacher;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableteacher = 'teachers';
      $this->tableuser = 'users';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableteacher, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableteacher, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableteacher, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableteacher);
      }
      public function getWithUser($parameterfilter=array()){
        $this->db->select("t.*, u.name, u.email, u.username");
        $this->db->from($this->tableteacher." t");
        $this->db->join($this->tableuser." u","t.user_id = u.id");
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get();
      }
      public function getTeacherSelect2($like){ // Menampilkan semua CALON dosen pengampu dari suatu matkul yang belum termasuk
        $this->db->select("t.id, u.name as text");
        $this->db->from($this->tableteacher." t");
        $this->db->join('users u','t.user_id = u.id');
        $this->db->where("(u.name LIKE '%$like%')");
        return $this->db->get();
      }
      public function getTeacher(){ // Menampilkan semua CALON dosen pengampu dari suatu matkul yang belum termasuk
        $this->db->select("t.id, u.name");
        $this->db->from($this->tableteacher." t");
        $this->db->join('users u','t.user_id = u.id');
        return $this->db->get();
      }

        function json() {
            $this->datatables->select('t.id, t.nip, u.name, u.email, t.card_id');
            $this->datatables->from($this->tableteacher.' t');
            $this->datatables->join($this->tableuser.' u','t.user_id = u.id');
            $url = base_url()."admin/editguru/";
              $this->datatables->add_column('view', '<center>
                        <a target="_blank" href="'.$url.'$1" class="btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>
                        <button type="button" onclick="remove($1)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></center>', 'id');
            return $this->datatables->generate();
        }
}
