<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$cek = $this->session->userdata('status');
		if($cek == 'admin'){
			header("location:".base_url()."admin");
		}
		else{
			$data = [
				'title' => 'Dashboard'
			];
			$this->load->view('login_v',$data);
		}

	}
	public function login(){
		$this->form_validation->set_rules('username', 'Username', 'required');
    $this->form_validation->set_rules('password', 'Password', 'required');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{
				$this->load->model(array('LoginModel'));
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$result = $this->LoginModel->login($username,$password);
				if($result == 1)
       		echo json_encode(['status'=>'success','message'=>'Login successful.']);
				else
					echo json_encode(['status'=>'error','message'=>'Username or password is invalid.']);
    }
	}
	public function logout(){
		$this->session->sess_destroy();
    header("location:".base_url());
	}
}
