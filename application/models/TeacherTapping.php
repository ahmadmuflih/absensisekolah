<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class TeacherTapping extends CI_Model {
  private $tableteachertap;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableteachertap = 'teachers_tapping';
      $this->tableteacher = 'teachers';
      $this->tableuser = 'users';
      $this->tableroom = 'rooms';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableteachertap, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableteachertap, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableteachertap, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableteachertap);
      }
      function json($teacher_id, $date) {
          $this->datatables->select("tt.id, t.nip, u.name, DATE_FORMAT(tt.clock_in, '%e-%m-%Y %T') as time,concat(r.code,' - ',r.name) as room");
          $this->datatables->from($this->tableteachertap.' tt');
          $this->datatables->join($this->tableteacher.' t','tt.teacher_id = t.id');
          $this->datatables->join($this->tableuser.' u','t.user_id = u.id');
          $this->datatables->join($this->tableroom.' r','tt.room_id = r.id','left');
          if($teacher_id!=0){
            $this->datatables->where('t.id',$teacher_id);
          }
          if($date!=0){
            $this->datatables->where('DATE(tt.clock_in)',$date);
          }

          $this->datatables->add_column('view', '<center>
                      <button type="button" onclick="remove($1)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></center>', 'id');
          return $this->datatables->generate();
      }
}
