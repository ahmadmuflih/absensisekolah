<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class LoginModel extends CI_Model {
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableuser = 'users';
      $this->tableadmin = 'admins';
    }
    public function login($usr, $pwd){
      $usr = trim($usr);
      $usr = $this->db->escape_str($usr);
      $cek_login =  $this->db->get_where($this->tableuser,array('username' => $usr));
      $cek = $cek_login->num_rows();
      if($cek>0){
        $akun = $cek_login->row();
        if (password_verify($pwd, $akun->password)) {
          $cek_admin = $this->cekAdmin(['user_id'=>$akun->id]);
          if($cek_admin->num_rows()>0){
            $sess = [
              'status' => 'admin',
              'name' => $akun->name,
              'email' => $akun->email
            ];
            $this->session->set_userdata($sess);
            return 1;
          }
        }
      }
      return 0;
    }


    private function cekAdmin($parameter = array()){
      $this->db->select('*');
      $this->db->from($this->tableadmin.' a');
      $this->db->where($parameter);
      return $this->db->get();
    }
}
