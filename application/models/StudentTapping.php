<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class StudentTapping extends CI_Model {
  private $tablestudenttap;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablestudenttap = 'students_tapping';
      $this->tablestudent = 'students';
      $this->tableuser = 'users';
      $this->tableroom = 'rooms';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablestudenttap, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablestudenttap, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablestudenttap, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablestudenttap);
      }
      function json($student_id, $date) {
          $this->datatables->select("st.id, s.nisn, u.name, DATE_FORMAT(st.clock_in, '%e-%m-%Y %T') as time, st.name as lesson, st.start_time, st.end_time, r.name as room");
          $this->datatables->from($this->tablestudenttap.' st');
          $this->datatables->join($this->tablestudent.' s','st.student_id = s.id');
          $this->datatables->join($this->tableuser.' u','s.user_id = u.id');
          $this->datatables->join($this->tableroom.' r','st.class_id = r.id','left');
          if($student_id!=0){
            $this->datatables->where('s.id',$student_id);
          }
          if($date!=0){
            $this->datatables->where('DATE(st.clock_in)',$date);
          }

          $this->datatables->add_column('view', '<center>
                      <button type="button" onclick="remove($1)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></center>', 'id');
          return $this->datatables->generate();
      }
}
