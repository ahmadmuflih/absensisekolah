<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Room extends CI_Model {
  private $tableroom;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableroom = 'rooms';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableroom, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableroom, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableroom, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableroom);
      }
      function json() {
          $this->datatables->select('r.id, r.code, r.name, rt.type');
          $this->datatables->from($this->tableroom.' r');
          $this->datatables->join('room_type rt','rt.id = r.type_id');
          $url = base_url()."admin/editruangan/";
            $this->datatables->add_column('view', '<center>
                      <a target="_blank" href="'.$url.'$1" class="btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>
                      <button type="button" onclick="remove($1)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></center>', 'id');
          return $this->datatables->generate();
      }
}
