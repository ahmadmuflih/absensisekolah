<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function tap_guru()
	{
		date_default_timezone_set("Asia/Jakarta");

			$room_code = $this->input->get('room_code');
			$card_id = $this->input->get('card_id');
			if(is_null($room_code) || is_null($card_id)){
				echo json_encode(['status'=>0,'message'=>['Wrong parameters']]);
				return;
			}
			$this->load->model(array('Room','Teacher','TeacherTapping'));
			$cek_ruangan = $this->Room->get(['code'=>$room_code]);
			if($cek_ruangan->num_rows()==0){
				echo json_encode(['status'=>0,'message'=>['Room is invalid']]);
				return;
			}
			$cek_guru = $this->Teacher->getWithUser(['card_id'=>$card_id]);
			if($cek_guru->num_rows()==0){
				echo json_encode(['status'=>0,'message'=>['Card is invalid']]);
				return;
			}
			$data = [
				'room_id' => $cek_ruangan->row()->id,
				'teacher_id' => $cek_guru->row()->id,
				'id_status' => 1,
				'clock_in' => date("Y-m-d H:i:s")
			];
			$this->TeacherTapping->insert($data);
			echo json_encode(['status'=>1,'message'=>[$cek_guru->row()->name,"Clockin ".date("H:i:s")]]);
	}

	public function tap_kelas()
	{
		date_default_timezone_set("Asia/Jakarta");

			$room_code = $this->input->get('room_code');
			$card_id = $this->input->get('card_id');
			if(is_null($room_code) || is_null($card_id)){
				echo json_encode(['status'=>0,'message'=>['Wrong parameters']]);
				return;
			}
			$this->load->model(array('Room','Teacher','Student','ClassSchedule','TeacherTapping','StudentTapping'));
			$cek_ruangan = $this->Room->get(['code'=>$room_code]);
			if($cek_ruangan->num_rows()==0){
				echo json_encode(['status'=>0,'message'=>['Room is invalid']]);
				return;
			}


			$cek = true;
			$cek_guru = $this->Teacher->getWithUser(['card_id'=>$card_id]);
			if($cek_guru->num_rows()==0){
				$cek = false;
			}
			else{
				$day = date('N', strtotime(date('l')));
				$time = date("H:i");
				$cek_schedule = $this->ClassSchedule->checkTeacherSchedule($cek_guru->row()->id,$cek_ruangan->row()->id,$day,$time);
				if($cek_schedule->num_rows()>0){
					$data = [
						'room_id' => $cek_ruangan->row()->id,
						'teacher_id' => $cek_guru->row()->id,
						'id_status' => 1,
						'clock_in' => date("Y-m-d H:i:s")
					];
					$this->TeacherTapping->insert($data);
					echo json_encode(['status'=>1,'message'=>[$cek_guru->row()->name,"Clockin ".date("H:i:s")]]);
				}
				else{
					echo json_encode(['status'=>0,'message'=>['No privilege']]);
				}
				return;
			}

			$cek_siswa = $this->Student->getWithUser(['card_id'=>$card_id]);
			if($cek_siswa->num_rows()==0){
				$cek = false;
			}else if($cek_siswa->row()->class_id != $cek_ruangan->row()->id){
				echo json_encode(['status'=>0,'message'=>['Wrong class!']]);
				return;
			}
			else{

				$day = date('N', strtotime(date('l')));
				$time = date("H:i");
				$cek_schedule = $this->ClassSchedule->checkStudentSchedule($cek_ruangan->row()->id,$day,$time);
				if($cek_schedule->num_rows()>0){
					$data = [
						'name' => $cek_schedule->row()->name,
						'start_time' => $cek_schedule->row()->start_time,
						'end_time' => $cek_schedule->row()->end_time,
						'clock_in' => date("Y-m-d H:i:s"),
						'student_id' => $cek_siswa->row()->id,
						'class_id' => $cek_ruangan->row()->id,
						'schedule_id' => $cek_schedule->row()->id
					];
					$this->StudentTapping->insert($data);
					echo json_encode(['status'=>1,'message'=>[$cek_siswa->row()->name,"Clockin ".date("H:i:s")]]);
				}else{
					echo json_encode(['status'=>0,'message'=>['No class now']]);
				}
				return;
			}


			if(!$cek){
				echo json_encode(['status'=>0,'message'=>['Card is invalid']]);
			}

	}

}
