<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AJAX extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('welcome_message');
	}
	public function getSchedule(){
		$class_id = $this->input->get('class_id');
		$this->load->model('ClassSchedule');
		$schedules = $this->ClassSchedule->getSchedule($class_id);
		$slot = [];
		foreach ($schedules->result() as $row) {
			$jam_mulai = (int)substr($row->start_time, 0, 2);
			$jam_selesai = (int)substr($row->end_time, 0, 2);
			for($j=$jam_mulai; $j<$jam_selesai;$j++){
				$slot[$row->day_id][$j] = "<b>$row->lesson <br> $row->teacher</b>";
			}
		}

		echo json_encode(['list'=>$schedules->result_array(),'slot'=>$slot]);
	}
	public function getScheduleDetail(){
		$schedule_id = $this->input->get('schedule_id');
		$this->load->model('ClassSchedule');
		$schedule = $this->ClassSchedule->get(['id'=>$schedule_id]);
		echo json_encode(['data'=>$schedule->row()]);
	}
	public function getTeacher(){
		$this->load->model('Teacher');
		$teachers = $this->Teacher->getTeacherSelect2($_GET['q']);
		echo json_encode($teachers->result());
	}
}
