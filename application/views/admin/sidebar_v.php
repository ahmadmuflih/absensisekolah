<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="<?php echo base_url() ?>admin">Absensi</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="index.html">St</a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Dashboard</li>
        <li <?php if(substr($page_id,0,1)=='1')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
        <li class="menu-header">Data</li>
        <li <?php if(substr($page_id,0,1)=='2')echo "class='active'";?> class="nav-item dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Master Data</span></a>
          <ul class="dropdown-menu">
            <li <?php if(substr($page_id,0,2)=='23')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/dataruangan">Master Data Ruangan</a></li>
            <li <?php if(substr($page_id,0,2)=='21')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/dataguru">Master Data Guru</a></li>
            <li <?php if(substr($page_id,0,2)=='22')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/datasiswa">Master Data Siswa</a></li>

          </ul>
        </li>
        <li <?php if(substr($page_id,0,1)=='4')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/jadwalkelas"><i class="fa fa-tasks"></i> <span>Jadwal Kelas</span></a></li>
        <li class="menu-header">Riwayat</li>
        <li  <?php if(substr($page_id,0,1)=='3')echo "class='active'";?> class="nav-item dropdown">
          <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-list"></i> <span>Riwayat Absensi</span></a>
          <ul class="dropdown-menu">
            <li  <?php if(substr($page_id,0,2)=='31')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/absensiguru">Absensi Guru</a></li>
            <li  <?php if(substr($page_id,0,2)=='32')echo "class='active'";?>><a class="nav-link" href="<?php echo base_url() ?>admin/absensisiswa">Absensi Siswa</a></li>
          </ul>
        </li>

      </ul>
  </aside>
</div>
