<?php
  $this->load->view('admin/header_v');
  $this->load->view('admin/sidebar_v') ?>
<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Master Data Ruangan</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
        <div class="breadcrumb-item"><a href="#">Master Data</a></div>
        <div class="breadcrumb-item">Data Ruangan</div>
      </div>
    </div>

    <div class="section-body">


      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="col-4">
                    <h4>List Ruangan</h4>
                </div>
                <div class="col-8">
                    <a href="<?php echo base_url() ?>admin/tambahruangan" class="btn btn-sm btn-primary float-right"><i class="fa fa-plus"></i> Add Data</a>
                </div>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="mytable">
                  <thead>
                    <tr>
                      <th class="text-center">
                        #
                      </th>
                      <th>Kode Ruangan</th>
                      <th>Nama Ruangan</th>
                      <th>Jenis Ruangan</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
loadData();
function loadData(){
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                      {
                          return {
                              "iStart": oSettings._iDisplayStart,
                              "iEnd": oSettings.fnDisplayEnd(),
                              "iLength": oSettings._iDisplayLength,
                              "iTotal": oSettings.fnRecordsTotal(),
                              "iFilteredTotal": oSettings.fnRecordsDisplay(),
                              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                          };
                      };

                      t = $("#mytable").DataTable({
                          initComplete: function() {
                              var api = this.api();
                              $('#mytable_filter input')
                                      .off('.DT')
                                      .on('keyup.DT', function(e) {
                                          if (e.keyCode == 13) {
                                              api.search(this.value).draw();
                                  }
                              });
                          },
                          oLanguage: {
                              sProcessing: "loading..."
                          },
                          processing: true,
                          serverSide: true,
                          select: true,
                          ajax: {"url": "<?php echo base_url("admin/json_ruangan");?>", "type": "POST"},
                          "columnDefs": [
              {
                  "targets": [ -1 ], //last column
                  "orderable": false, //set not orderable
              },
              ],
                          columns: [
                              {
                                  "data": "id",
                                  "orderable": false
                              },
                              {"data": "code"},
                              {"data": "name"},
                              {"data": "type"},
                              {"data": "view"}
                          ],
                          order: [[1, 'asc']],
                          rowCallback: function(row, data, iDisplayIndex) {
                              var info = this.fnPagingInfo();
                              var page = info.iPage;
                              var length = info.iLength;
                              var index = page * length + (iDisplayIndex + 1);
                              $('td:eq(0)', row).html(index);
                          }
                      });


          }
          function remove(id) {
        swal({
            title: 'Yakin?',
            text: 'Data ruangan akan terhapus!',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url : "<?php echo base_url() ?>admin/deleteruangan",
                    type: "POST",
                    dataType: "JSON",
                    data:{'id':id},
                    success: function(data)
                    {
                        if (data.status=='success') {
                            swal("Success!", data.message, "success");
                            reload_table();
                        }else {
                            swal("Failed!", data.message, "error");
                        }
                        console.log();
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        console.log(jqXHR);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {

            }
        });
    }
    function reload_table()
{
  $('#mytable').DataTable().ajax.reload(null,false); //reload datatable ajax
}
</script>

<?php $this->load->view('admin/footer_v'); ?>
