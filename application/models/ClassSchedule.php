<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class ClassSchedule extends CI_Model {
  private $tableschedule;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tableschedule = 'class_schedules';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tableschedule, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tableschedule, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tableschedule, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tableschedule);
      }
      public function checkClass($day_id, $class_id, $start_time, $end_time, $id = null){
        if($id==null){
          return $this->db->query("SELECT * FROM `class_schedules` WHERE day_id = $day_id and class_id = $class_id
          and ((start_time <= '$start_time' and end_time > '$start_time')
                OR
               (start_time < '$end_time' and end_time >= '$end_time')
               OR
               (start_time >= '$start_time' and end_time <= '$end_time'));");
         }
         else{
           return $this->db->query("SELECT * FROM `class_schedules` WHERE day_id = $day_id and class_id = $class_id and id != $id
           and ((start_time <= '$start_time' and end_time > '$start_time')
                 OR
                (start_time < '$end_time' and end_time >= '$end_time')
                OR
                (start_time >= '$start_time' and end_time <= '$end_time'));");
         }
      }

      public function checkTeacherSchedule($teacher_id,$class_id,$day_id,$time){
        return $this->db->query("SELECT * FROM `class_schedules` WHERE teacher_id = $teacher_id AND class_id = $class_id AND day_id = $day_id AND start_time <= '$time' AND end_time >= '$time';");
      }
      public function checkStudentSchedule($class_id,$day_id,$time){
        return $this->db->query("SELECT * FROM `class_schedules` WHERE class_id = $class_id AND day_id = $day_id AND start_time <= '$time' AND end_time >= '$time';");
      }

      public function getSchedule($class_id){
        $this->db->select("s.id, s.name as lesson, s.day_id, s.start_time, s.end_time, d.name_indo as day, r.name as room, u.name as teacher");
        $this->db->from($this->tableschedule." s");
        $this->db->join("days d","s.day_id = d.id");
        $this->db->join("teachers t","s.teacher_id = t.id","left");
        $this->db->join("users u","t.user_id = u.id","left");
        $this->db->join("rooms r","s.class_id = r.id");
        $this->db->where("s.class_id",$class_id);
        $this->db->order_by("s.day_id",'ASC');
        return $this->db->get();
      }
}
