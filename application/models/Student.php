<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Student extends CI_Model {
  private $tablestudent;
  function __construct(){
      parent::__construct();
      // $this->db1 = $this->load->database('db1', TRUE);
      $this->tablestudent = 'students';
      $this->tableuser = 'users';
    }


    public function insert($arraydata = array() )
    {
      $this->db->insert($this->tablestudent, $arraydata);
      $last_recore = $this->db->insert_id();
      return $last_recore;
    }
    public function update($parameterfilter=array(), $arraydata=array() )
      {
          $this->db->where($parameterfilter);
          $this->db->update($this->tablestudent, $arraydata);
          return $this->db->affected_rows();
      }
      public function delete($parameter=array())
      {
          $this->db->delete($this->tablestudent, $parameter );
          return $this->db->affected_rows();
      }
      public function get($parameterfilter=array()){
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get($this->tablestudent);
      }
      public function getWithUser($parameterfilter=array()){
        $this->db->select("s.*, u.name, u.email, u.username");
        $this->db->from($this->tablestudent." s");
        $this->db->join($this->tableuser." u","s.user_id = u.id");
        if($parameterfilter!=null)
        $this->db->where($parameterfilter);
        return $this->db->get();
      }
      function json($class) {
          $this->datatables->select('s.id, s.nisn, u.name, u.email, s.card_id, r.name as class');
          $this->datatables->from($this->tablestudent.' s');
          $this->datatables->join($this->tableuser.' u','s.user_id = u.id');
          $this->datatables->join('rooms r','s.class_id = r.id');
          if($class!="0")
            $this->datatables->where('r.id',$class);
          $url = base_url()."admin/editsiswa/";
            $this->datatables->add_column('view', '<center>
                      <a sarget="_blank" href="'.$url.'$1" class="btn btn-sm btn-primary"><i class="fas fa-pencil-alt"></i></a>
                      <button sype="button" onclick="remove($1)" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button></center>', 'id');
          return $this->datatables->generate();
      }
}
