<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
 function __construct(){
    parent::__construct();
		$cek = $this->session->userdata('status');
		if($cek != 'admin'){
			header("location:".base_url());
		}
	}
	public function index()
	{
		$data = [
			'page_id' => '1',
			'title' => 'Dashboard'
		];
		$this->load->view('admin/dashboard_v',$data);
	}

 // ---------------------------

	public function dataguru()
	{
		$data = [
			'page_id' => 21,
			'title' => 'Master Data Guru'
		];
		$this->load->view('admin/dataguru/dataguru_v',$data);
	}
	public function tambahguru()
	{
		$data = [
			'page_id' => 21,
			'title' => 'Tambah Data Guru'
		];
		$this->load->view('admin/dataguru/tambahguru_v',$data);
	}

	function json_guru() {
		$this->load->library('datatables');
		$this->load->model(array('Teacher'));
		header('Content-Type: application/json');
		echo $this->Teacher->json();
	}

	public function editguru($id = null)
	{
		if(is_null($id)){
			header("location:".base_url()."admin/dataguru");
		}
		$this->load->model(array('Teacher','User'));
		$teacher = $this->Teacher->get(['id'=>$id]);
		if($teacher->num_rows()==0){
			header("location:".base_url()."admin/dataguru");
		}
		$user = $this->User->get(['id'=>$teacher->row()->user_id]);

		$data = [
			'page_id' => 21,
			'title' => 'Edit Data Guru',
			'teacher' => $teacher->row(),
			'user' => $user->row()
		];
		$this->load->view('admin/dataguru/editguru_v',$data);
	}
	public function insertguru(){
		$this->form_validation->set_rules('nip', 'NIP', 'required|is_unique[teachers.nip]');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[6]|is_unique[users.username]');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('card_id', 'Card ID', 'required|is_unique[teachers.card_id]');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{
				$this->load->model(array('Teacher','User'));
				$user = [
					'name' => $this->input->post('name'),
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email'),
					'password' => password_hash($this->input->post('username'), PASSWORD_BCRYPT)
				];
				$user_id = $this->User->insert($user);
				$teacher = [
					'user_id' => $user_id,
					'nip' => $this->input->post('nip'),
					'card_id' => $this->input->post('card_id'),
				];
				$this->Teacher->insert($teacher);
				echo json_encode(['status'=>'success','message'=>'Data guru berhasil ditambahkan!.']);
    }
	}
	public function updateguru(){
		$teacher_id = $this->input->post('teacher_id');
		if(is_null($teacher_id)){
			echo json_encode(['status'=>'error','message'=>'Tidak ada teacher id']);
			return;
		}
		$this->load->model(array('Teacher','User'));
		$check_teacher = $this->Teacher->get(['id'=>$teacher_id]);
		if($check_teacher->num_rows()==0){
			echo json_encode(['status'=>'error','message'=>'Guru tidak ditemukan!']);
			return;
		}
		$datateacher = $check_teacher->row();
		$user_id = $datateacher->user_id;
		$this->form_validation->set_rules('nip', 'NIP', 'required|edit_unique[teachers.nip.'.$teacher_id.']');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[6]|edit_unique[users.username.'.$user_id.']');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email|edit_unique[users.email.'.$user_id.']');
		$this->form_validation->set_rules('card_id', 'Card ID', 'required|edit_unique[teachers.card_id.'.$teacher_id.']');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{

				$user = [
					'name' => $this->input->post('name'),
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email')
				];
				$this->User->update(['id'=>$user_id],$user);
				$teacher = [
					'nip' => $this->input->post('nip'),
					'card_id' => $this->input->post('card_id'),
				];
				$this->Teacher->update(['id'=>$teacher_id],$teacher);
				echo json_encode(['status'=>'success','message'=>'Data guru berhasil diupdate!.']);
    }
	}
	function deleteguru(){
		$this->load->model(array('Teacher','User'));
		$param = [
			'id'=>$this->input->post('id')
		];
		$cek = $this->Teacher->get($param);
		if($cek->num_rows()>0){
			$this->User->delete(['id'=>$cek->row()->user_id]);
			echo json_encode(['status'=>'success','message'=>'Guru berhasil dihapus!']);
		}
		else{
			echo json_encode(['status'=>'error','message'=>'Guru tidak ditemukan!']);
		}
	}

  // ---------------------------

  public function datasiswa()
	{
    $this->load->model('Room');
		$data = [
			'page_id' => 22,
			'title' => 'Master Data Siswa',
      'class' => $this->Room->get(['type_id'=>1])
		];
		$this->load->view('admin/datasiswa/datasiswa_v',$data);
	}
	public function tambahsiswa()
	{
    $this->load->model('Room');
		$data = [
			'page_id' => 22,
			'title' => 'Tambah Data Siswa',
      'class' => $this->Room->get(['type_id'=>1])
		];
		$this->load->view('admin/datasiswa/tambahsiswa_v',$data);
	}

  function json_siswa() {
		$this->load->library('datatables');
		$this->load->model(array('Student'));
		header('Content-Type: application/json');
    $class = $this->input->post('class_id');
		echo $this->Student->json($class);
	}

  public function editsiswa($id = null)
	{
		if(is_null($id)){
			header("location:".base_url()."admin/datasiswa");
		}
		$this->load->model(array('Student','User','Room'));
		$student = $this->Student->get(['id'=>$id]);
		if($student->num_rows()==0){
			header("location:".base_url()."admin/datasiswa");
		}
		$user = $this->User->get(['id'=>$student->row()->user_id]);

		$data = [
			'page_id' => 22,
			'title' => 'Edit Data Siswa',
			'student' => $student->row(),
			'user' => $user->row(),
      'class' => $this->Room->get(['type_id'=>1])
		];
		$this->load->view('admin/datasiswa/editsiswa_v',$data);
	}

  public function insertsiswa(){
		$this->form_validation->set_rules('nisn', 'NISN', 'required|is_unique[students.nisn]');
		$this->form_validation->set_rules('name', 'Nama', 'required');
    $this->form_validation->set_rules('class', 'Kelas', 'required');
		$this->form_validation->set_rules('card_id', 'Card ID', 'required|is_unique[students.card_id]');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{
				$this->load->model(array('Student','User'));
				$user = [
					'name' => $this->input->post('name'),
					'username' => "",
					'email' => $this->input->post('email'),
					'password' => ""
				];
				$user_id = $this->User->insert($user);
				$student = [
					'user_id' => $user_id,
					'nisn' => $this->input->post('nisn'),
          'class_id' => $this->input->post('class'),
					'card_id' => $this->input->post('card_id'),
				];
				$this->Student->insert($student);
				echo json_encode(['status'=>'success','message'=>'Data siswa berhasil ditambahkan!.']);
    }
	}

  public function updatesiswa(){
		$student_id = $this->input->post('student_id');
		if(is_null($student_id)){
			echo json_encode(['status'=>'error','message'=>'Tidak ada student id']);
			return;
		}
		$this->load->model(array('Student','User'));
		$check_student = $this->Student->get(['id'=>$student_id]);
		if($check_student->num_rows()==0){
			echo json_encode(['status'=>'error','message'=>'Siswa tidak ditemukan!']);
			return;
		}
		$datastudent = $check_student->row();
		$user_id = $datastudent->user_id;
		$this->form_validation->set_rules('nisn', 'NISN', 'required|edit_unique[students.nisn.'.$student_id.']');
		$this->form_validation->set_rules('name', 'Nama', 'required');
    $this->form_validation->set_rules('class', 'Kelas', 'required');
		$this->form_validation->set_rules('card_id', 'Card ID', 'required|edit_unique[teachers.card_id.'.$student_id.']');

    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{

				$user = [
					'name' => $this->input->post('name'),
					'username' => "",
					'email' => $this->input->post('email')
				];
				$this->User->update(['id'=>$user_id],$user);
				$student = [
					'nisn' => $this->input->post('nisn'),
          'class_id' => $this->input->post('class'),
					'card_id' => $this->input->post('card_id'),
				];
				$this->Student->update(['id'=>$student_id],$student);
				echo json_encode(['status'=>'success','message'=>'Data siswa berhasil diupdate!.']);
    }
	}
	function deletesiswa(){
		$this->load->model(array('Student','User'));
		$param = [
			'id'=>$this->input->post('id')
		];
		$cek = $this->Student->get($param);
		if($cek->num_rows()>0){
			$this->User->delete(['id'=>$cek->row()->user_id]);
			echo json_encode(['status'=>'success','message'=>'Siswa berhasil dihapus!']);
		}
		else{
			echo json_encode(['status'=>'error','message'=>'Siswa tidak ditemukan!']);
		}

	}

	// ---------------------------

  public function jadwalkelas()
	{
    $this->load->model(['Room','Teacher']);
		$data = [
			'page_id' => 4,
			'title' => 'Jadwal Kelas',
      'class' => $this->Room->get(['type_id'=>1]),
      'teachers' => $this->Teacher->getTeacher()
		];
		$this->load->view('admin/jadwalkelas/index_v',$data);
	}

  public function insertjadwal(){
    // print_r($_POST);
    $this->form_validation->set_rules('class_id', 'Kelas', 'required');
		$this->form_validation->set_rules('name', 'Nama', 'required');
    $this->form_validation->set_rules('day', 'Hari', 'required');
    $this->form_validation->set_rules('start_time', 'Waktu mulai', 'required');
    $this->form_validation->set_rules('end_time', 'Waktu selesai', 'required');
    // $this->form_validation->set_rules('teacher', 'Guru', 'required');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{
				$this->load->model(array('ClassSchedule'));

				$schedule = [
					'name' => $this->input->post('name'),
					'start_time' => $this->input->post('start_time'),
          'end_time' => $this->input->post('end_time'),
					'day_id' => $this->input->post('day'),
          'class_id' => $this->input->post('class_id'),
					'teacher_id' => $this->input->post('teacher'),
				];

        if($schedule['start_time'] >= $schedule['end_time']){
          echo json_encode(['status'=>'failed','message'=>'Data waktu tidak valid']);
          return;
        }
        $check = $this->ClassSchedule->checkClass($schedule['day_id'],$schedule['class_id'],$schedule['start_time'],$schedule['end_time']);
        if($check->num_rows()>0){
          echo json_encode(['status'=>'failed','message'=>'Jam tersebut berhalangan dengan mata pelajaran lain!']);
          return;
        }

        $this->ClassSchedule->insert($schedule);
				echo json_encode(['status'=>'success','message'=>'Data jadwal kelas berhasil ditambahkan!.']);
    }
	}
  public function updatejadwal(){
    // print_r($_POST);
    $this->form_validation->set_rules('id', 'ID', 'required');
    $this->form_validation->set_rules('class_id', 'Kelas', 'required');
		$this->form_validation->set_rules('name', 'Nama', 'required');
    $this->form_validation->set_rules('day', 'Hari', 'required');
    $this->form_validation->set_rules('start_time', 'Waktu mulai', 'required');
    $this->form_validation->set_rules('end_time', 'Waktu selesai', 'required');
    // $this->form_validation->set_rules('teacher', 'Guru', 'required');
    if ($this->form_validation->run() == FALSE){
        $errors = validation_errors();
        echo json_encode(['status'=>'error','message'=>$errors]);
    }else{
				$this->load->model(array('ClassSchedule'));

				$schedule = [
					'name' => $this->input->post('name'),
					'start_time' => $this->input->post('start_time'),
          'end_time' => $this->input->post('end_time'),
					'day_id' => $this->input->post('day'),
          'class_id' => $this->input->post('class_id'),
					'teacher_id' => $this->input->post('teacher'),
				];

        $param = [
          'id' => $this->input->post('id'),
        ];

        if($schedule['start_time'] >= $schedule['end_time']){
          echo json_encode(['status'=>'failed','message'=>'Data waktu tidak valid']);
          return;
        }
        $check = $this->ClassSchedule->checkClass($schedule['day_id'],$schedule['class_id'],$schedule['start_time'],$schedule['end_time'],$param['id']);
        if($check->num_rows()>0){
          echo json_encode(['status'=>'failed','message'=>'Jam tersebut berhalangan dengan mata pelajaran lain!']);
          return;
        }

        $this->ClassSchedule->update($param,$schedule);
				echo json_encode(['status'=>'success','message'=>'Data jadwal kelas berhasil diupdate!.']);
    }
	}

  function deletejadwal(){
		$this->load->model(array('ClassSchedule','User'));
		$param = [
			'id'=>$this->input->post('id')
		];
		$cek = $this->ClassSchedule->get($param);
		if($cek->num_rows()>0){
			$this->ClassSchedule->delete($param);
			echo json_encode(['status'=>'success','message'=>'Jadwal berhasil dihapus!']);
		}
		else{
			echo json_encode(['status'=>'error','message'=>'Jadwal tidak ditemukan!']);
		}

	}

  // ---------------------------

 	public function dataruangan()
 	{
 		$data = [
 			'page_id' => 23,
 			'title' => 'Master Data Ruangan'
 		];
 		$this->load->view('admin/dataruangan/dataruangan_v',$data);
 	}
 	public function tambahruangan()
 	{
 		$data = [
 			'page_id' => 23,
 			'title' => 'Tambah Data Ruangan',
 		];
 		$this->load->view('admin/dataruangan/tambahruangan_v',$data);
 	}

 	function json_ruangan() {
 		$this->load->library('datatables');
 		$this->load->model(array('Room'));
 		header('Content-Type: application/json');
 		echo $this->Room->json();
 	}

 	public function editruangan($id = null)
 	{
 		if(is_null($id)){
 			header("location:".base_url()."admin/dataruangan");
 		}
 		$this->load->model(array('Room'));
 		$room = $this->Room->get(['id'=>$id]);
 		if($room->num_rows()==0){
 			header("location:".base_url()."admin/dataruangan");
 		}

 		$data = [
 			'page_id' => 23,
 			'title' => 'Edit Data Guru',
 			'room' => $room->row()
 		];
 		$this->load->view('admin/dataruangan/editruangan_v',$data);
 	}
 	public function insertruangan(){
 		$this->form_validation->set_rules('name', 'Nama', 'required');
 		$this->form_validation->set_rules('code', 'Kode Ruangan', 'required|min_length[5]|is_unique[rooms.code]');
    $this->form_validation->set_rules('type', 'Jenis Ruangan', 'required');
     if ($this->form_validation->run() == FALSE){
         $errors = validation_errors();
         echo json_encode(['status'=>'error','message'=>$errors]);
     }else{
 				$this->load->model(array('Room'));
 				$room = [
 					'name' => $this->input->post('name'),
 					'code' => $this->input->post('code'),
          'type_id' => $this->input->post('type'),
 				];
 				$this->Room->insert($room);
 				echo json_encode(['status'=>'success','message'=>'Data ruangan berhasil ditambahkan!.']);
     }
 	}
 	public function updateruangan(){
 		$room_id = $this->input->post('room_id');
 		if(is_null($room_id)){
 			echo json_encode(['status'=>'error','message'=>'Tidak ada room id']);
 			return;
 		}
 		$this->form_validation->set_rules('name', 'Nama', 'required');
 		$this->form_validation->set_rules('code', 'Kode Ruangan', 'required|min_length[5]|edit_unique[rooms.code.'.$room_id.']');
    $this->form_validation->set_rules('type', 'Jenis Ruangan', 'required');
     if ($this->form_validation->run() == FALSE){
         $errors = validation_errors();
         echo json_encode(['status'=>'error','message'=>$errors]);
     }else{
			 $this->load->model(array('Room'));
			 $room = [
				 'name' => $this->input->post('name'),
				 'code' => $this->input->post('code'),
         'type_id' => $this->input->post('type'),
			 ];
			 $this->Room->update(['id'=>$room_id],$room);
			 echo json_encode(['status'=>'success','message'=>'Data ruangan berhasil ditambahkan!.']);
     }
 	}
 	function deleteruangan(){
 		$this->load->model(array('Room'));
 		$param = [
			'id'=>$this->input->post('id')
		];
 		$cek = $this->Room->delete(['id'=>$param['id']]);
 		if($cek>0){
 			echo json_encode(['status'=>'success','message'=>'Ruangan berhasil dihapus!']);
 		}
 		else{
 			echo json_encode(['status'=>'error','message'=>'Ruangan tidak ditemukan!']);
 		}
 	}

	//-------------------------

	public function absensiguru()
	{
		$this->load->Model(array('Teacher'));
		$teacher = $this->Teacher->getWithUser();
		$data = [
			'page_id' => 31,
			'title' => 'Absensi Guru',
			'teacher' => $teacher
		];
		$this->load->view('admin/absensiguru_v',$data);
	}

	function json_absensi_guru() {
		$this->load->library('datatables');
		$this->load->model(array('TeacherTapping'));
		$teacher_id = $this->input->post('teacher_id');
		$date = $this->input->post('date');
		header('Content-Type: application/json');
		echo $this->TeacherTapping->json($teacher_id,$date);
	}

	function deleteabsensiguru(){
 		$this->load->model(array('TeacherTapping'));
 		$param = [
			'id'=>$this->input->post('id')
		];
 		$cek = $this->TeacherTapping->delete(['id'=>$param['id']]);
 		if($cek>0){
 			echo json_encode(['status'=>'success','message'=>'Absensi berhasil dihapus!']);
 		}
 		else{
 			echo json_encode(['status'=>'error','message'=>'Absensi tidak ditemukan!']);
 		}
 	}
  //-------------------------

	public function absensisiswa()
	{
		$this->load->Model(array('Student'));
		$student = $this->Student->getWithUser();
		$data = [
			'page_id' => 32,
			'title' => 'Absensi Siswa',
			'student' => $student
		];
		$this->load->view('admin/absensisiswa_v',$data);
	}

  function json_absensi_siswa() {
		$this->load->library('datatables');
		$this->load->model(array('StudentTapping'));
		$student_id = $this->input->post('student_id');
		$date = $this->input->post('date');
		header('Content-Type: application/json');
		echo $this->StudentTapping->json($student_id,$date);
	}

  function deleteabsensisiswa(){
 		$this->load->model(array('StudentTapping'));
 		$param = [
			'id'=>$this->input->post('id')
		];
 		$cek = $this->StudentTapping->delete(['id'=>$param['id']]);
 		if($cek>0){
 			echo json_encode(['status'=>'success','message'=>'Absensi berhasil dihapus!']);
 		}
 		else{
 			echo json_encode(['status'=>'error','message'=>'Absensi tidak ditemukan!']);
 		}
 	}


}
